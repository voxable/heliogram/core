# frozen_string_literal: true

module Heliogram
  class Worker
    include Sidekiq::Worker

    sidekiq_options retry: 3

    # Find the appropriate user for this request.
    #
    # @param [Class] bot
    #   The class representing the bot in question.
    # @param [String, Integer] user_id
    #   The id of the user to fetch.
    #
    # @return [Object] The user that initiated the request.
    private def find_bot_user(bot, user_id)
      # TODO: High - this belongs in Heliogram::Smooch
      bot.user_class.find_or_create_by(smooch_id: user_id)
    end

    # Pop a message from the passed queue.
    #
    # @param [Class] queue_class
    #   The class representing the queue from which
    #   to pop.
    # @param [String, Integer] user_id
    #   The ID representing the user on this platform
    # @param [String] namespace
    #   The redis namespace under which the message to
    #   process is nested.
    #
    # @return [Hash] The raw, popped message.
    private def pop_from_queue(queue_class, user_id:, namespace:)
      queue_class
        .new(user_id: user_id, namespace: namespace)
        .pop
    end

    # Parse a message via an NLU service (at the moment, Dialogflow).
    #
    # @param [String] text
    #   The raw text of the message.
    # @param [String] user_id
    #   The platform-specific ID of the user that sent the message.
    # @param [API] user
    #   The user that sent the message.
    #
    # @return [Hash]
    #   The raw NLU client response.
    private def parse_message(text, user_id, user)
      begin
        # Gather user context
        user_log_context = {
          user: {
            id: user_id,
            meta: {
              conversation_state:       user.conversation_state,
              dialogflow_context_name:  user.dialogflow_context_name
            }
          }
        }

        # ...send the message to API.ai for NLU.
        nlu_response = Dialogflow::Client.new(user.api_ai_session_id)
                         .query(text,
                                context_name: user.dialogflow_context_name,
                                log_context: user_log_context)

        # Clear the Dialogflow context.
        user.update_attributes(dialogflow_context_name: nil) if user.dialogflow_context_name

        return nlu_response
      rescue Heliogram::Dialogflow::Client::QueryError => e
        log_error(e, user_log_context)
      end
    end

    # Log an error.
    #
    # @param [Error] e
    #   The error to log.
    # @param [Timber::Contexts::User] context
    #   User context for Timber
    #
    # @return [void]
    private def log_error(e, context)
      Timber.with_context context do
        logger.error e.message
        logger.error e.backtrace.join()
      end
    end

    # Initialize a new Chatbase API client for this worker.
    #
    # @param [String] intent
    #   Determined intent of user message
    # @param [String] text
    #   Text of user message
    # @param [Boolean] not_handled
    #   Flag for user message not understood
    #
    # @return [Heliogram::ChatbaseAPIClient]
    #   The Chatbase API client.
    private def initialize_chatbase_api_client(intent, text, not_handled)
      @chatbase_api_client ||= ChatbaseAPIClient.new(
        intent: intent,
        text: text,
        not_handled: not_handled
      )
    end

    # Method to set Chatbase client fields
    #
    # @param [String] intent
    #   Determined intent of user message
    # @param [String] text
    #   Text of user message
    # @param [Boolean] not_handled
    #   Flag for user message not understood
    #
    # @return [void]
    private def set_chatbase_fields(intent, text, not_handled)
      return unless ChatbaseAPIClient.api_key

      initialize_chatbase_api_client(intent, text, not_handled)
    end

    # Method to send user message to Chatbase
    #
    # @param [String] intent
    #   Determined intent of user message
    # @param [String] text
    #   Text of user message
    # @param [Boolean] not_handled
    #   Flag for user message not understood
    # @param [Hash] message
    #   Message or postback received
    #
    # @return [void]
    private def send_user_message(intent: nil, text: nil, not_handled: nil, message:)
      return unless ChatbaseAPIClient.api_key

      Thread.new do
        initialize_chatbase_api_client(intent, text, not_handled)

        @chatbase_api_client.send_user_message message
      end
    end
  end
end
