# frozen_string_literal: true

# TODO: Test & Document
module Heliogram
  module Dialogflow
    class Client
      class QueryError < StandardError
        def initialize
          super('There was a problem with the Dialogflow query.')
        end
      end

      # Name of action for unrecognized messages.
      UNKNOWN_SYSTEM_ACTION = 'input.unknown'

      def initialize(session_id)
        # TODO: Enable setting environment variable in bot config.
        @client = Google::Cloud::Dialogflow::Sessions.new
        @session = @client.class.session_path (ENV['DIALOGFLOW_PROJECT'] || ENV['GCP_PROJECT_ID']), session_id
      end

      # Parse a natural language message with API.ai.
      #
      # @param message [String]
      #   The natural language query to be parsed.
      # @param context_name [String]
      #   The context name to set on the Dialogflow session.
      # @param log_context [Hash]
      #   Context to include in logging
      #
      # @return [Hash] The information parsed from the message.
      def query(message, context_name: nil, log_context: nil)
        # TODO: which logger?
        retry_counter = 0

        begin
          retry_counter += 1

          # Build a contexts object if one is passed.
          # TODO: Get this functional again.
          contexts = context_name ? [context_name] : nil

          query_input = { text: { text: message, language_code: 'en-US' } }
          dialogflow_response = @client.detect_intent(@session, query_input)
        rescue Google::Gax::GaxError => e
          Timber.with_context log_context do
            Sidekiq::Logging.logger.error 'Error with Dialogflow request'
            Sidekiq::Logging.logger.error e
            Sidekiq::Logging.logger.error e.backtrace.join("\n")
          end

          # Retry the call 3 times.
          retry if retry_counter < 3

          raise QueryError.new
        else
          dialogflow_response
          intent_name = dialogflow_response.query_result.intent.display_name

          # Determine the action name
          action_from_response = dialogflow_response.query_result.action

          # Use intent name as action name if no action name is defined for this intent.
          action_name =
            if action_from_response.blank?
              intent_name
              # Set to default action if message is not recognized.
            elsif action_from_response == UNKNOWN_SYSTEM_ACTION.freeze
              Heliogram::InternalActions::DEFAULT
            else
              action_from_response
            end

          # TODO: Allow handling of multiple fulfillment messages.
          fulfillment = dialogflow_response.query_result.fulfillment_messages

          return {
            intent: intent_name,
            action: action_name,
            parameters: parsed_params(dialogflow_response.query_result.parameters),
            fulfillment: fulfillment
          }
        end
      end

      private

      # Correctly format params object.
      #
      # @param params [Google::Protobuf::Struct]
      #   The raw parsed params.
      #
      # @return [Hash]
      #   Only those params that were matched.
      def parsed_params(params)
        formatted_params = {}
        params.fields.collect do |k, v|
          # TODO: Dialogflow upgrade - support all param types
          formatted_params[k] = params[k]
        end

        formatted_params.reject!{ |p| p.blank? }
        formatted_params
      end
    end
  end
end
