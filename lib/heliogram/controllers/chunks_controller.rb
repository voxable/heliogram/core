# frozen_string_literal: true

# TODO: High - remove
require 'heliogram/smooch/controller'

module Heliogram
  # All controllers internal to Heliogram. These handle the internal routes,
  # like 'Heliogram::Actions::DISPLAY_CHUNK'.
  module Controllers
    # TODO: Document & test
    # TODO: High - Abstract this so it's not tied to Smoooch.
    class ChunksController < Heliogram::Smooch::Controller
      def display_chunk
        # TODO: Shouldn't be constantizing user input. Need a way to sanitize this.
        # Although - payloads shouldn't be something the client is allowed to set.
        chunk_class = Kernel.const_get(params[:chunk])

        respond_with chunk_class
      end
    end
  end
end
