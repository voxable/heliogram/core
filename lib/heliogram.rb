# frozen_string_literal: true

require 'google/cloud/dialogflow'
require 'sidekiq'

require 'heliogram/version'

require_dependency 'heliogram/dialogflow/client'

require_dependency 'heliogram/controller'
require_dependency 'heliogram/controllers/chunks_controller'
require_dependency 'heliogram/internal_actions'
require_dependency 'heliogram/queue'
require_dependency 'heliogram/redis'
require_dependency 'heliogram/request'
require_dependency 'heliogram/router'
require_dependency 'heliogram/worker'

module Heliogram
  # Your code goes here...
end
